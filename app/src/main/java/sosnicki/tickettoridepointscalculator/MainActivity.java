package sosnicki.tickettoridepointscalculator;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import sosnicki.tickettoridepointscalculator.Game.Color;
import sosnicki.tickettoridepointscalculator.Game.Colors;
import sosnicki.tickettoridepointscalculator.Game.Game;
import sosnicki.tickettoridepointscalculator.Game.GameConfiguration;
import sosnicki.tickettoridepointscalculator.Game.IScenario;
import sosnicki.tickettoridepointscalculator.Game.NordicCountriesScenario;
import sosnicki.tickettoridepointscalculator.ImageProcessing.IRouteProcessor;
import sosnicki.tickettoridepointscalculator.ImageProcessing.RouteProcessor;
import sosnicki.tickettoridepointscalculator.Player.Player;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback  {


    private static final String TAG="MainActivity";
    private static final int RESULT_LOAD_IMG =  32;
    private static final int RESULT_TAKE_PHOTO =  34;
    private static final int SHOW_DETAILS_CODE =  12;

    Button loadImageButton;
    Button takePictureButton;

    IScenario scenario;

    EditText pinkPlayerNameText;
    EditText purplePlayerNameText;

    static {
        if(OpenCVLoader.initDebug()){
            Log.d(TAG, "OpenCV successfully loaded");
        }
        else{
            Log.d(TAG, "OpenCV not loaded");
        }
    }



//    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
//        @Override
//        public void onManagerConnected(int status) {
//            switch (status) {
//                case LoaderCallbackInterface.SUCCESS:
//                {
//                    Log.i(TAG, "OpenCV loaded successfully");
//                } break;
//
//                default:
//                {
//                    Log.i(TAG, "OpenCV loaded FAILURE");
//                    super.onManagerConnected(status);
//                } break;
//            }
//        }
//    };
//
//    public void onResume()
//    {
//        super.onResume();
//        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, this, mLoaderCallback);
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }



    private Mat readImageFromResources(int resourceId) {
        try {
            Mat img = null;
            img = Utils.loadResource(this, resourceId);
            return resizeImage(img);
        } catch (IOException e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        return null;
    }

    @NonNull
    private Mat resizeImage(Mat img) {
        int divider = 2;
        Mat result = new Mat(img.rows()/divider, img.cols()/divider, img.type());
        Imgproc.resize(img, result, result.size(), 0,0, Imgproc.INTER_CUBIC);


        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        String[] PERMISSIONS = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        };

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
        }


        IRouteProcessor processor = new RouteProcessor
                (readImageFromResources(R.drawable.bottom_left), readImageFromResources(R.drawable.bottom_right),
                        readImageFromResources(R.drawable.top_left), readImageFromResources(R.drawable.top_right));
        scenario = new NordicCountriesScenario();

        GameConfiguration configuration =
                new GameConfiguration(Arrays.asList(Colors.pinkTrainColor,
                        Colors.purpleTrainColor), scenario);

        CurrentGameData.currentGame =   new Game(configuration, processor);

        loadImageButton = findViewById(R.id.LoadImageButton);

        loadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getIntentForImageFromGallery();
            }
        });

        takePictureButton = findViewById(R.id.TakePhotoButton);

        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getIntentForImageFromCamera();
            }
        });


        pinkPlayerNameText = findViewById(R.id.PinkPlayerNameBox);
        purplePlayerNameText = findViewById(R.id.PurplePlayerNameBox);
    }


    private void showGameDetails(){
        Intent i = new Intent(this, GameDetailsActivity.class);
        startActivityForResult(i, SHOW_DETAILS_CODE);
    }



    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    void getIntentForImageFromCamera(){
        setPlayerNames();
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "sosnicki.tickettoridepointscalculator.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, RESULT_TAKE_PHOTO);
            }
        }
    }

    void getIntentForImageFromGallery(){
        setPlayerNames();
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
    }

    private void setPlayerNames() {
        String pinkPlayerName = pinkPlayerNameText.getText().toString();
        String purplePlayerName = purplePlayerNameText.getText().toString();

        if(pinkPlayerName.isEmpty())
            pinkPlayerName = "Pink";
        if(purplePlayerName.isEmpty())
            purplePlayerName = "Purple";

        if(pinkPlayerName.contentEquals(purplePlayerName))
        {
            pinkPlayerName+="1";
            purplePlayerName+="2";
        }

        CurrentGameData.currentGame.setPlayerName(Colors.pinkTrainColor, pinkPlayerName);
        CurrentGameData.currentGame.setPlayerName(Colors.purpleTrainColor, purplePlayerName);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode==RESULT_OK && requestCode == RESULT_LOAD_IMG) {

            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                Mat result = new Mat();
                Utils.bitmapToMat(selectedImage, result);
                Imgproc.cvtColor(result, result, Imgproc.COLOR_RGBA2BGR);
                result = resizeImage(result);
                if(result.width()>result.height())
                {
                    Core.rotate(result,result,Core.ROTATE_90_CLOCKWISE);
                }
                CurrentGameData.currentImage = result.clone();

                showGameDetails();


            }
            catch (Exception e) {
            }
        }
        if(resultCode==RESULT_OK && requestCode == RESULT_TAKE_PHOTO) {
            try {
                final Bitmap selectedImage = BitmapFactory.decodeFile(mCurrentPhotoPath);
                Mat result = new Mat();
                Utils.bitmapToMat(selectedImage, result);
                Imgproc.cvtColor(result, result, Imgproc.COLOR_RGBA2BGR);
                result = resizeImage(result);
                if(result.width()>result.height())
                {
                    Core.rotate(result,result,Core.ROTATE_90_CLOCKWISE);
                }
                CurrentGameData.currentImage = result.clone();
                showGameDetails();
            }
            catch (Exception e) {
            }
        }
    }
}

