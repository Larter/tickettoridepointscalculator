package sosnicki.tickettoridepointscalculator;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.github.chrisbanes.photoview.PhotoView;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import sosnicki.tickettoridepointscalculator.Game.Game;
import sosnicki.tickettoridepointscalculator.Player.Player;

public class GameDetailsActivity extends AppCompatActivity {


    private static final int SHOW_RESULTS_CODE = 5;
    private static final int ADD_ROUTES_CODES = 6;
    private static final int ADD_TICKETS_CODE = 7;

    Spinner playerSpinner_;

    Button addTicketButtton_;
    Button addRoutesButton_;
    Button backButton_;
    Button showResultsButton_;

    PhotoView photoView_;

    List<String> playerNames_;
    ArrayAdapter<String> playerNamesAdapter_;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_details);

        playerSpinner_ = findViewById(R.id.PlayerSpinner);
        addRoutesButton_ = findViewById(R.id.AddRouteButton);
        addTicketButtton_ = findViewById(R.id.AddTicketButton);
        backButton_ = findViewById(R.id.BackButton);
        showResultsButton_ = findViewById(R.id.ShowResultsButton);
        photoView_ = findViewById(R.id.PhotoImageView);
        CurrentGameData.currentGame.loadPointsFromImage(CurrentGameData.currentImage);
        updateImage(CurrentGameData.currentGame.getImage());
//        updateImage(CurrentGameData.currentImage);

        playerNames_ = new ArrayList<>();
        for (Player player : CurrentGameData.currentGame.getPlayers()) {
            playerNames_.add(player.getName());
        }
        playerNamesAdapter_ = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, playerNames_);
        playerSpinner_.setAdapter(playerNamesAdapter_);


        addRoutesButton_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addRoutes();
            }
        });

        addTicketButtton_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTickets();
            }
        });

        showResultsButton_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showResults();
            }
        });

        backButton_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    private void addTickets() {
        Intent i = new Intent(this, AddTicket.class);
        i.putExtra("Player", getSelectedPlayerName());
        startActivityForResult(i, ADD_TICKETS_CODE);
    }


    String getSelectedPlayerName() {
        return playerNames_.get(playerSpinner_.getSelectedItemPosition());
    }


    private void addRoutes() {
        //Not yet implemented
    }


    private void showResults() {
        Intent i = new Intent(this, ShowPointsResult.class);
        startActivityForResult(i, SHOW_RESULTS_CODE);
    }

    private void updateImage(Mat img) {
        Imgproc.cvtColor(img, img, Imgproc.COLOR_RGB2BGRA);
        Bitmap bm = Bitmap.createBitmap(img.cols(), img.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(img, bm);
        photoView_.setImageBitmap(bm);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == SHOW_RESULTS_CODE) {

            if(data.getExtras().containsKey("mainMenu"))
                finish();
        }
    }
}