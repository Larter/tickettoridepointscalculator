package sosnicki.tickettoridepointscalculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.opencv.core.Scalar;

import sosnicki.tickettoridepointscalculator.Game.Color;
import sosnicki.tickettoridepointscalculator.Game.ResultText;

public class ShowPointsResult extends AppCompatActivity {

    TextView player1NameTextView;
    TextView player1ResultTextView;
    TextView player1SumTextView;

    TextView player2NameTextView;
    TextView player2ResultTextView;
    TextView player2SumTextView;


    TextView winnerTextView;


    Button backButton;
    Button mainMenuButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_points_result);

        player1NameTextView = findViewById(R.id.Player1NameTextView);
        player1ResultTextView = findViewById(R.id.Player1PointsResultTextView);
        player1SumTextView = findViewById(R.id.Player1PointsSumTextView);

        player2NameTextView = findViewById(R.id.Player2NameTextView);
        player2ResultTextView = findViewById(R.id.Player2PointsResultTextView);
        player2SumTextView = findViewById(R.id.Player2PointsSumTextView);

        winnerTextView = findViewById(R.id.WinnerTextView);

        backButton = findViewById(R.id.BackButton);
        mainMenuButton = findViewById(R.id.MainMenuButton);

        ResultText result = CurrentGameData.currentGame.getResultText();

        player1NameTextView.setText(result.playerResults.get(0).playerName);

        player1NameTextView.setBackgroundColor(
                CurrentGameData.currentGame.getPlayer(result.playerResults.get(0).playerName)
                        .getColor().getHashCode());
        player1ResultTextView.setText(result.playerResults.get(0).resultText);
        player1SumTextView.setText(result.playerResults.get(0).resultSum);

        player2NameTextView.setText(result.playerResults.get(1).playerName);
        player2NameTextView.setBackgroundColor(
                CurrentGameData.currentGame.getPlayer(result.playerResults.get(1).playerName)
                        .getColor().getHashCode());

        player2ResultTextView.setText(result.playerResults.get(1).resultText);
        player2SumTextView.setText(result.playerResults.get(1).resultSum);

        winnerTextView.setText(result.winnerName);

        Color color = CurrentGameData.currentGame.getPlayer(result.winnerName).getColor();
        winnerTextView.setBackgroundColor(color.getHashCode());

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mainMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent data = new Intent();
                data.putExtra("mainMenu", "Yes");
                setResult(RESULT_OK, data);
                finish();
            }
        });

    }

}
