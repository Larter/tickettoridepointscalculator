package sosnicki.tickettoridepointscalculator.ImageProcessing;

import android.support.annotation.NonNull;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;

import sosnicki.tickettoridepointscalculator.Game.Color;
import sosnicki.tickettoridepointscalculator.Game.RelativePosition;
import sosnicki.tickettoridepointscalculator.Game.RouteDetails;
import sosnicki.tickettoridepointscalculator.Player.Player;

import static org.opencv.imgproc.Imgproc.TM_CCORR_NORMED;

/**
 * Created by Marek Sośnicki on 2018-05-01.
 */

public class RouteProcessor implements IRouteProcessor{

    private Mat image_;
    private Mat original_;


    private Mat bottomLeftPicture_;
    private Mat bottomRightPicture_;
    private Mat topLeftPicture_;
    private Mat topRightPicture_;

    private Point bottomLeft_;
    private Point bottomRight_;
    private Point topLeft_;
    private Point topRight_;
    private List<Player> players_;


    Point getMatch(Mat templ, boolean isRight, boolean isBottom) {
        int result_cols = image_.cols() - templ.cols() + 1;
        int result_rows = image_.rows() - templ.rows() + 1;
        Mat result = new Mat(result_rows, result_cols, CvType.CV_32FC1);

        // / Do the Matching and Normalize
        Imgproc.matchTemplate(image_, templ, result, TM_CCORR_NORMED);
        Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new Mat());

        // / Localizing the best match with minMaxLoc
        Core.MinMaxLocResult mmr = Core.minMaxLoc(result);

        int xChange = 0;
        int yChange = 0;
        if (isRight)
            xChange = templ.cols();
        if (isBottom)
            yChange = templ.rows();

        return new Point(mmr.maxLoc.x + xChange, mmr.maxLoc.y + yChange);

    }


    public RouteProcessor( Mat bottomLeft, Mat bottomRight, Mat topLeft, Mat topRight) {
         bottomLeftPicture_ = bottomLeft;
         bottomRightPicture_ = bottomRight;
         topLeftPicture_ =  topLeft;
         topRightPicture_=  topRight;
    }

    @Override
    public void loadImage(Mat image, List<Player> players) {
        image_ = image.clone();
        original_ = image.clone();

        bottomLeft_ = getMatch(bottomLeftPicture_, false, true);
        bottomRight_ = getMatch(bottomRightPicture_, true, true);
        topLeft_ = getMatch(topLeftPicture_, false, false);
        topRight_ = getMatch(topRightPicture_, true, false);
        players_ = players;
        markCorners();
    }

    public void markCorners() {
        Imgproc.circle(image_, bottomLeft_, 20, new Scalar(0, 0, 255), 20);
        Imgproc.circle(image_, bottomRight_, 20, new Scalar(0, 0, 255), 20);
        Imgproc.circle(image_, topLeft_, 20, new Scalar(0, 0, 255), 20);
        Imgproc.circle(image_, topRight_, 20, new Scalar(0, 0, 255), 20);
    }


    public Mat markPosition(RelativePosition relativePosition, Color markingColor) {

        Point found = getRelativePositionPoint(relativePosition);
        int squareSize = 12;
        Imgproc.rectangle(image_,
                new Point(found.x - squareSize, found.y - squareSize),
                new Point(found.x + squareSize, found.y + squareSize),
        new Scalar(255, 255, 255), squareSize*2);
//                markingColor.getMainColor(), squareSize*2);

        Imgproc.circle(image_, found, 10, markingColor.getMainColor(), 15);
//
//        String strValue = "{" + String.valueOf(Math.round((relativePosition.getPercentageX() * 100))) + " " +
//                String.valueOf(Math.round((relativePosition.getPercentageY() * 100))) + "}";
//
//
//        Imgproc.putText(image_, strValue, found, Core.FONT_HERSHEY_SIMPLEX, 0.3f, new Scalar(0,0,0), 1);
        return image_;
    }

    public Mat getImage() {
        return image_;
    }

    public Player checkPlayer(RouteDetails details) {
        Player dummyPlayer = new Player(details.getColor());

        HashMap<Player, Integer> playerHashMap = new HashMap<>();
        for (Player player : players_) {
            playerHashMap.put(player, 0);
        }
        playerHashMap.put(dummyPlayer, 0);


        for (RelativePosition relativePosition : details.getPositions()) {

            HashMap<Player, Integer> localPlayerHashMap = new HashMap<>();
            for (Player player : players_) {
                localPlayerHashMap.put(player, 0);
            }
            localPlayerHashMap.put(dummyPlayer, 0);

            Point position = getRelativePositionPoint(relativePosition);
            int squareSize = 20;

            drawFrameOfSearchArea(position, squareSize);


            int xCenter = (int) position.x;
            int yCenter = (int) position.y;

            int xMin = Math.max(xCenter-squareSize, 0);
            int yMin = Math.max(yCenter-squareSize, 0);
            int xMax = Math.min(xCenter+squareSize, original_.width());
            int yMax = Math.min(yCenter+squareSize, original_.height());

            for (int x = xMin; x < xMax; ++x) {
                for (int y = yMin; y < yMax; ++y) {
                    double[] color = original_.get(y, x);

                    double distanceToOriginalRouteColor = dummyPlayer.getColor().getDistance(color);

                    Player bestPlayer = null;

                    if(distanceToOriginalRouteColor<10) {
                        bestPlayer = dummyPlayer;
                    }
                    for (Player player : players_) {
                        double playerDistanceToColor = player.getColor().getDistance(color);

                        if (playerDistanceToColor < 10 &&
                                playerDistanceToColor < distanceToOriginalRouteColor) {
                            bestPlayer = player;
                            distanceToOriginalRouteColor = playerDistanceToColor;
                        }
                    }
                    if(bestPlayer != null)
                    {
                        localPlayerHashMap.put(bestPlayer, localPlayerHashMap.get(bestPlayer) + 1);
                    }
                }
            }


            int bestValue = localPlayerHashMap.get(dummyPlayer);
            Player bestPlayer = dummyPlayer;
            for(Player player: players_)
            {
                if(localPlayerHashMap.get(player)>= bestValue && localPlayerHashMap.get(player)>0)
                {
                    bestPlayer = player;
                    bestValue = localPlayerHashMap.get(player);
                }
            }
            playerHashMap.put(bestPlayer, playerHashMap.get(bestPlayer)+1);
        }

        int bestValue = playerHashMap.get(dummyPlayer);
        Player bestPlayer = dummyPlayer;
        for (Player player : players_) {

            if(playerHashMap.get(player)>= bestValue)
            {
                bestPlayer = player;
                bestValue = playerHashMap.get(player);
            }
        }

        if(bestPlayer==dummyPlayer)
            return null;

        Color markingColor = bestPlayer==null? details.getColor() : bestPlayer.getColor();
        for(RelativePosition position: details.getPositions()) {
            markPosition(position, markingColor);
        }

        return bestPlayer == dummyPlayer ? null : bestPlayer;
    }



    private void drawFrameOfSearchArea(Point position, int squareSize) {
        int xCenter = (int) position.x;
        int yCenter = (int) position.y;
        final double[] black = {0.0, 0.0, 0.0};


        int xMin = Math.max(xCenter-squareSize, 0);
        int yMin = Math.max(yCenter-squareSize, 0);
        int xMax = Math.min(xCenter+squareSize+1, original_.width());
        int yMax = Math.min(yCenter+squareSize+1, original_.height());

        for (int x = xMin; x < xMax; ++x) {
            for (int y = yMin; y < yMax; y+=squareSize) {
                image_.put(y,x,black);
            }
        }

        for (int x = xMin; x < xMax; x+=squareSize) {
            for (int y = yMin; y < yMax; y++) {
                image_.put(y,x,black);
            }
        }
    }

    @NonNull
    private Point getRelativePositionPoint(RelativePosition relativePosition) {
        double width = topRight_.x - topLeft_.x;
        double heigh = bottomLeft_.y - topLeft_.y;
        return new Point(topLeft_.x + width * relativePosition.getPercentageX(),
                topLeft_.y + heigh * relativePosition.getPercentageY());
    }


}
