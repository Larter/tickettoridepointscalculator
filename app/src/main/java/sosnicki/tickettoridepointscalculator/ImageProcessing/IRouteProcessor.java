package sosnicki.tickettoridepointscalculator.ImageProcessing;

import org.opencv.core.Mat;

import java.util.List;

import sosnicki.tickettoridepointscalculator.Game.RouteDetails;
import sosnicki.tickettoridepointscalculator.Player.Player;

/**
 * Created by Marek Sośnicki on 2018-05-05.
 */

public interface IRouteProcessor {
    Player checkPlayer(RouteDetails details);
    void loadImage(Mat image, List<Player> players);
    Mat getImage();
}
