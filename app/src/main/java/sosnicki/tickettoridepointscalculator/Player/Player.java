package sosnicki.tickettoridepointscalculator.Player;

import org.opencv.core.Scalar;

import java.util.Comparator;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.TreeSet;

import javax.xml.transform.Result;

import sosnicki.tickettoridepointscalculator.Game.Color;


/**
 * Created by Marek Sośnicki on 2018-04-30.
 */

public class Player {

    private Color color_;
    private String name_;
    private Set<Route> routes_;
    private Set<Ticket> tickets_;

    private Map<String, Set<String>> cityConnections;


    static final Map<Integer, Integer> pointsPerRoute = createValuesMap();

    private static Map<Integer, Integer> createValuesMap() {
        Map<Integer, Integer> result = new HashMap<>();
        result.put(1, 1);
        result.put(2, 2);
        result.put(3, 4);
        result.put(4, 7);
        result.put(5, 10);
        result.put(6, 15);
        result.put(9, 27);
        return result;
    }

    public Player(Color color, String name) {
        color_ = color;
        name_ = name;
        resetValues();
    }


    public Player(Color color) {
        color_ = color;
        name_ = "dummy";
        resetValues();
    }

    private void resetValues() {
        routes_ = new HashSet<Route>();
        tickets_ = new TreeSet<>(new Comparator<Ticket>() {
            @Override
            public int compare(Ticket ticket, Ticket t1) {
                return ticket.toString().compareTo(t1.toString());
            }
        });
        cityConnections = new HashMap<String, Set<String>>();
    }

    public Color getColor() {
        return color_;
    }


    public PointsResult GetResult() {

        int pointsFromRoutes = 0;
        int pointsFromTickets = 0;

        for (Route route : routes_) {
            pointsFromRoutes += pointsPerRoute.get(route.getLength());
        }

        int noOfFinishedTickets = 0;

        for (Ticket ticket : tickets_) {
            if (cityConnections.containsKey(ticket.getFrom())
                    && cityConnections.get(ticket.getFrom()).contains(ticket.getTo())) {
                pointsFromTickets += ticket.getValue();
                noOfFinishedTickets++;
            } else {
                pointsFromTickets -= ticket.getValue();
            }
        }

        return new PointsResult(pointsFromRoutes, pointsFromTickets, noOfFinishedTickets);
    }

    public void addRoute(Route route) {
        routes_.add(route);
        Set<String> connectedCities = new HashSet<>();
        connectedCities.add(route.getTo());
        connectedCities.add(route.getFrom());

        if (cityConnections.containsKey(route.getFrom()))
            connectedCities.addAll(cityConnections.get(route.getFrom()));
        if (cityConnections.containsKey(route.getTo()))
            connectedCities.addAll(cityConnections.get(route.getTo()));

        for (String connectionKey : connectedCities)
            cityConnections.put(connectionKey, connectedCities);
    }

    public void addTicket(Ticket ticket) {
        tickets_.add(ticket);
    }

    public Set<Ticket> getTickets()
    {
        return tickets_;
    }

    public void removeTicket(Ticket ticket) {
        if(tickets_.contains(ticket))
            tickets_.remove(ticket);
    }

    public void reset() {
        resetValues();
    }

    public String getName()
    {
        return name_;
    }

    public void setName(String name)
    {
        name_ = name;
    }
}
