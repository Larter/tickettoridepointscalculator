package sosnicki.tickettoridepointscalculator.Player;

/**
 * Created by Marek on 2018-04-30.
 */

public class Ticket {
    private String from_;
    private String to_;
    private int value_;

    public Ticket(String from, String to, int  value)
    {
        from_=from;
        to_ = to;
        value_ = value;
    }

    public String getFrom() {
        return from_;
    }

    public String getTo() {
        return to_;
    }

    public int getValue() {
        return value_;
    }

    @Override
    public String toString() { return from_ + "-" + to_ + " : " + String.valueOf(value_);}
}
