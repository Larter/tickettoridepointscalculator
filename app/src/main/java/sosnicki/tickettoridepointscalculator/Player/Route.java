package sosnicki.tickettoridepointscalculator.Player;

/**
 * Created by Marek on 2018-04-30.
 */

public class Route {
    private String from_;
    private String to_;
    private int length_;

    public Route(String from, String to, int length)
    {
        from_=from;
        to_ = to;
        length_ = length;
    }

    public String getFrom() {
        return from_;
    }

    public String getTo() {
        return to_;
    }

    public int getLength() {
        return length_;
    }

}
