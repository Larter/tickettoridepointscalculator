package sosnicki.tickettoridepointscalculator.Player;

/**
 * Created by Marek Sośnicki on 2018-04-30.
 */

public class PointsResult {
    private int pointsFromRoutes_;
    private int pointsFromTickets_;
    private int noOfFinishedTickets_;

    public PointsResult(int pointsFromRoutes, int pointsFromTickets, int noOfFinishedTickets)
    {
        pointsFromRoutes_ = pointsFromRoutes;
        pointsFromTickets_ = pointsFromTickets;
        noOfFinishedTickets_ = noOfFinishedTickets;
    }

    public int getPointsFromRoutes() {
        return pointsFromRoutes_;
    }

    public int getPointsFromTickets() {
        return pointsFromTickets_;
    }

    public int getNoOfFinishedTickets() {
        return noOfFinishedTickets_;
    }
}
