package sosnicki.tickettoridepointscalculator.Game;

import org.opencv.core.Mat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import sosnicki.tickettoridepointscalculator.ImageProcessing.IRouteProcessor;
import sosnicki.tickettoridepointscalculator.Player.Player;
import sosnicki.tickettoridepointscalculator.Player.PointsResult;
import sosnicki.tickettoridepointscalculator.Player.Route;
import sosnicki.tickettoridepointscalculator.Player.Ticket;

/**
 * Created by Marek Sośnicki on 2018-05-01.
 */

public class Game
{
    List<Player> players_;
    IRouteProcessor processor_;
    IScenario scenario_;

    public Game(GameConfiguration configuration, IRouteProcessor processor)
    {
        players_ = new ArrayList<>();
        for (Color color: configuration.getPlayers())
        {
            players_.add(new Player(color));
        }

        scenario_ = configuration.getScenario();
        processor_ = processor;
    }

    public PointsResult getResult(Color playerColor)
    {
        for(Player player: players_)
        {
            if(playerColor == player.getColor())
            {
                return player.GetResult();
            }
        }
        return new PointsResult(0,0, 0);
    }


    public Map<String, Set<String>> getPossibleTickets()
    {
        Map<String, Set<String>> result = new TreeMap<>();

        for(Ticket ticket : scenario_.getTickets())
        {
            if(!result.containsKey(ticket.getFrom()))
                result.put(ticket.getFrom(), new TreeSet<String>());
            result.get(ticket.getFrom()).add(ticket.getTo());
        }

        return result;
    }


    public void setPlayerName(Color color, String name)
    {
        for(Player player: players_)
        {
            if(player.getColor() == color)
            {
                player.setName(name);
                return;
            }
        }

    }

    public Player getPlayer(String name)
    {
        for(Player player: players_)
        {
            if(player.getName().contentEquals(name))
            {
                return player;
            }
        }
        return null;
    }

    public void setRouteStatus(String from, String to, Color baseColor, Color playerColor)
    {

    }

    public void loadPointsFromImage(Mat image)
    {

        for (Player player : players_)
        {
            player.reset();
        }

        processor_.loadImage(image, players_);

        for(RouteDetails details: scenario_.getRoutes())
        {
            Player player = processor_.checkPlayer(details);
            if(player!=null)
            {
                player.addRoute(new Route(details.getFrom(), details.getTo(), details.getPositions().size()));
            }
        }
    }


    public Ticket getTicket(String from, String to) {
        for(Ticket ticket: scenario_.getTickets())
        {
            if(ticket.getFrom().contentEquals(from) && ticket.getTo().contentEquals(to))
            {
                return ticket;
            }
        }
        return null;
    }

    public ResultText getResultText()
    {
        ResultText result = new ResultText();

        Player playerWithMostTickets = players_.get(0);

        int mostTickets = 0;
        List<PointsResult> playerResults = new ArrayList<>();


        for(Player player : players_)
        {
            ResultTextForPlayer textForPlayer = new ResultTextForPlayer();
            PointsResult pointsResult = player.GetResult();

            playerResults.add(pointsResult);
            textForPlayer.resultText = "";
            textForPlayer.resultText+= String.format("Points from routes : %d\n", pointsResult.getPointsFromRoutes());
            textForPlayer.resultText+= String.format("Points from tickets : %d\n", pointsResult.getPointsFromTickets());
            textForPlayer.playerName = player.getName();

            if(pointsResult.getNoOfFinishedTickets() > mostTickets)
            {
                mostTickets = pointsResult.getNoOfFinishedTickets();
                playerWithMostTickets = player;
            } else if(pointsResult.getNoOfFinishedTickets() == mostTickets)
            {
                playerWithMostTickets = null;
            }
            result.playerResults.add(textForPlayer);
        }

        Player bestPlayer = players_.get(0);
        int bestResult = 0;
        for(int i=0; i<players_.size();++i)
        {
            int pointsValue = 0;
            if(players_.get(i)==playerWithMostTickets)
            {
                pointsValue+=10;
                result.playerResults.get(i).resultText+="Globthrother bonus! : 10\n";
            }
            pointsValue+=playerResults.get(i).getPointsFromRoutes();
            pointsValue+=playerResults.get(i).getPointsFromTickets();
            if(pointsValue>bestResult)
            {
                bestPlayer = players_.get(i);
                bestResult = pointsValue;
            }
            result.playerResults.get(i).resultSum= String.valueOf(pointsValue);

        }
        result.winnerName = bestPlayer.getName();


        return result;
    }

    public List<Player> getPlayers()
    {
        return players_;
    }

    public Mat getImage()
    {
        return processor_.getImage();
    }
}
