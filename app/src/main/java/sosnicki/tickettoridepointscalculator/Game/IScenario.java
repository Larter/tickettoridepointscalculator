package sosnicki.tickettoridepointscalculator.Game;

import java.util.List;

import sosnicki.tickettoridepointscalculator.Player.Ticket;

/**
 * Created by Marek Sośnicki on 2018-05-01.
 */

public interface IScenario {

    List<RouteDetails> getRoutes();
    List<Ticket> getTickets();
}
