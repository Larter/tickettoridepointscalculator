package sosnicki.tickettoridepointscalculator.Game;

import org.opencv.core.Mat;
import org.opencv.core.Scalar;

import java.util.List;

/**
 * Created by Marek Sośnicki on 2018-05-01.
 */

public class GameConfiguration
{
    private List<Color> players_;
    private IScenario scenario_;
    public GameConfiguration(List<Color> players, IScenario scenario)
    {
        players_ =players;
        scenario_ = scenario;
    }
    public IScenario getScenario() { return  scenario_;}
    public List<Color> getPlayers() {
        return players_;
    }
}
