package sosnicki.tickettoridepointscalculator.Game;

/**
 * Created by Marek Sośnicki on 2018-05-01.
 */

public class RelativePosition {

    private double percentageX_;
    private double percentageY_;

    public RelativePosition(double percentageX, double percentageY)
    {
        percentageX_ = percentageX;
        percentageY_ = percentageY;
    }

    public double getPercentageX() {
        return percentageX_;
    }

    public double getPercentageY() {
        return percentageY_;
    }
}
