package sosnicki.tickettoridepointscalculator.Game;


import org.opencv.core.Scalar;

import java.util.List;

/**
 * Created by Marek Sośnicki on 2018-05-01.
 */

public class RouteDetails {
    private String from_;
    private String to_;
    private Color color_; //Change to something else
    private List<RelativePosition> positions_;

    public RouteDetails(String from, String to, Color color, List<RelativePosition> positions) {
        from_ = from;
        to_ = to;
        color_ = color;
        positions_ = positions;
    }

    public String getFrom() {
        return from_;
    }

    public String getTo() {
        return to_;
    }


    public Color getColor() {
        return color_;
    }

    public List<RelativePosition> getPositions() {
        return positions_;
    }
}
