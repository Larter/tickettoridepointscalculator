package sosnicki.tickettoridepointscalculator.Game;

import org.opencv.core.Mat;
import org.opencv.core.Scalar;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Marek Sośnicki on 2018-05-05.
 */

public class Color {

    private List<Scalar> values_;
    private String name_;

    public Color(String name, List<Scalar> values)
    {
        values_ = values;
        name_ = name;
    }

    public Color(String name, Scalar value)
    {
        values_ = new ArrayList<>();
        values_.add(value);
        name_ = name;
    }

    public double getDistance(double[] color) {
        double minimum = 1000;
        for (Scalar value : values_) {
//            double distance = Math.abs(color[0] - value.val[0])
//                    + Math.abs(color[1] - value.val[1])
//                    + Math.abs(color[2] - value.val[2]);

            double distance = Math.max(Math.abs(color[0] - value.val[0]),Math.abs(color[1] - value.val[1]) );
            distance = Math.max(distance, Math.abs(color[2] - value.val[2]));
            if (distance < minimum)
                minimum = distance;
        }
        return minimum;
    }

    public Scalar getMainColor()
    {
        return values_.get(0);
    }

    public String toString()
    {
        return name_;
    }

    public int getHashCode()
    {
        Scalar color = values_.get(0);
        long result = 0xFF000000;
        result |= Math.round(color.val[2]) <<16;
        result|= Math.round(color.val[1]) <<8;
        result|= Math.round(color.val[0]);
        return (int) result;
    }

}
