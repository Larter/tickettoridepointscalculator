package sosnicki.tickettoridepointscalculator.Game;

import org.opencv.core.Scalar;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Marek Sośnicki on 2018-05-02.
 */

public class Colors {

    // B G R


    static public class Route
    {
        public static final Color Red = new Color("Route::Red",
                new ArrayList<>(Arrays.asList(
                        new Scalar(30,40,180),
                        new Scalar(85,100,185),
                        new Scalar(60,80,190))));

        public static final Color Black = new Color("Route::Black",
                new ArrayList<>(Arrays.asList(
                        new Scalar(80,80,80),
                        new Scalar(55,70,95),
                        new Scalar(35,35,35),
                        new Scalar(80,85,95),
                        new Scalar(40,50,60))));

        public static final Color Orange = new Color("Route::Orange",
                new ArrayList<>(Arrays.asList(
                        new Scalar(50,130,210),
                        new Scalar(35,100,190),
                        new Scalar(20,90,170))));

        public static final Color Purple = new Color("Route::Purple",
                new ArrayList<>(Arrays.asList(
                        new Scalar(150,110,160),
                        new Scalar(65,75,130),
                        new Scalar(140,130,190),
                        new Scalar(125,125,180),
                        new Scalar(146,100,140))));


        public static final Color Green = new Color("Route::Green",
                new ArrayList<>(Arrays.asList(
                        new Scalar(50,180,145),
                        new Scalar(15,80,70),
                        new Scalar(20,140,120),
                        new Scalar(30,155,145),
                        new Scalar(15,115,95))));

        public static final Color White = new Color("Route::White",
                new ArrayList<>(Arrays.asList(
                        new Scalar(215,215,215),
                        new Scalar(250,250,250),
                        new Scalar(175,185,190))));

        public static final Color Blue = new Color("Route::Blue",
                new ArrayList<>(Arrays.asList(
                        new Scalar(210,180,50),
                        new Scalar(120,80,10),
                        new Scalar(200,150,0))));
        public static final Color Yellow = new Color("Route::Yellow",
                new ArrayList<>(Arrays.asList(
                        new Scalar(35,200,220),
                        new Scalar(30,160,200),
                        new Scalar(30,140,190))));

        public static final Color Neutral = new Color("Route::Neutral",
                new ArrayList<>(Arrays.asList(
                        new Scalar(160,160,160),
                        new Scalar(60,60,90),
                        new Scalar(180,165,145),
                        new Scalar(130,155,180),

                        new Scalar(184,166,185))));

    }

    static public class Ferry
    {
        public static final Color Red = new Color("Ferry::Red",
                new ArrayList<>(Arrays.asList(
                        new Scalar(30,40,180),
                        new Scalar(70,105,180),
                        new Scalar(15, 30,160),
                        new Scalar(10,20,120),
                        new Scalar(85,100,185),
                        new Scalar(60,80,190))));

        public static final Color Black = new Color("Ferry::Black",
                new ArrayList<>(Arrays.asList(
                        new Scalar(80,80,80),
                        new Scalar(55,70,95),
                        new Scalar(80,85,95),
                        new Scalar(40,50,60))));

        public static final Color Orange = new Color("Route::Orange",
                new ArrayList<>(Arrays.asList(
                        new Scalar(50,130,210),
                        new Scalar(35,100,190),
                        new Scalar(100,155,190),
                        new Scalar(55,85,130),
                        new Scalar(20,90,170))));

        public static final Color Green = new Color("Ferry::Green",
                new ArrayList<>(Arrays.asList(
                        new Scalar(50,180,145),
                        new Scalar(15,80,70),
                        new Scalar(15,80,70),
                        new Scalar(20,140,120),
                        new Scalar(105,170,165),
                        new Scalar(100,190,170),
                        new Scalar(25,90,50))));


        public static final Color Purple = new Color("Ferry::Purple",
                new ArrayList<>(Arrays.asList(
                        new Scalar(180,140,190),
                        new Scalar(100,60,110),
                        new Scalar(140,130,190),
                        new Scalar(125,125,180),
                        new Scalar(165,170,195),
                        new Scalar(80,80,135),
                        new Scalar(200,185,205))));
        public static final Color White = new Color("Ferry::White",
                new ArrayList<>(Arrays.asList(
                        new Scalar(215,215,215),
                        new Scalar(250,250,250),
                        new Scalar(100,130,155),
                        new Scalar(175,185,190))));

        public static final Color Blue = new Color("Ferry::Blue",
                new ArrayList<>(Arrays.asList(
                        new Scalar(210,180,50),
                        new Scalar(200,150,0),
                        new Scalar(120,80,10),
                        new Scalar(165,145,100),
                        new Scalar(130,95,30))));

        public static final Color Yellow = new Color("Ferry::Yellow",
                new ArrayList<>(Arrays.asList(
                        new Scalar(35,200,220),
                        new Scalar(30,160,200),
                        new Scalar(15,200,240),
                        new Scalar(10,200,220),
                        new Scalar(30,140,190))));
        public static final Color Neutral = new Color("Ferry::Neutral",
                new ArrayList<>(Arrays.asList(
                        new Scalar(160,160,160),
                        new Scalar(60,60,90),
                        new Scalar(130,155,180),
                        new Scalar(175,160,140),
                        new Scalar(100,125,103),
                        new Scalar(184,166,185))));
    }

    static public class Tunnel
    {
        public static final Color Red = new Color("Tunnel::Red",
                new ArrayList<>(Arrays.asList(
                        new Scalar(30,40,180),
                        new Scalar(10,15,55),
                        new Scalar(15,20,90),
                        new Scalar(30,55,165),
                        new Scalar(85,100,185),
                        new Scalar(60,80,190))));


        public static final Color Purple = new Color("Tunnel::Purple",
                new ArrayList<>(Arrays.asList(
                        new Scalar(180,140,190),
                        new Scalar(95,80,105),
                        new Scalar(140,130,190),
                        new Scalar(125,125,180),
                        new Scalar(135,95,160))));

        public static final Color Black = new Color("Tunnel::Black",
                new ArrayList<>(Arrays.asList(
                        new Scalar(80,80,80),
                        new Scalar(55,70,95),
                        new Scalar(80,85,95),
                        new Scalar(40,50,60))));

        public static final Color Orange = new Color("Tunnel::Orange",
                new ArrayList<>(Arrays.asList(
                        new Scalar(50,130,210),
                        new Scalar(35,100,190),
                        new Scalar(20,90,170))));

        public static final Color Green = new Color("Tunnel::Green",
                new ArrayList<>(Arrays.asList(
                        new Scalar(50,180,145),
                        new Scalar(45,165,140),
                        new Scalar(20,140,120),
                        new Scalar(50,115,95))));


        public static final Color Blue = new Color("Tunnel::Blue",
                new ArrayList<>(Arrays.asList(
                        new Scalar(210,180,50),
                        new Scalar(120,80,10),
                        new Scalar(200,150,0))));


        public static final Color White = new Color("Tunnel::White",
                new ArrayList<>(Arrays.asList(
                        new Scalar(215,215,215),
                        new Scalar(90,115,130),
                        new Scalar(105,135,165),
                        new Scalar(250,250,250),
                        new Scalar(175,185,190),
                        new Scalar(104,110,125))));


        public static final Color Yellow = new Color("Tunnel::Yellow",
                new ArrayList<>(Arrays.asList(
                        new Scalar(35,200,220),
                        new Scalar(30,160,200),
                        new Scalar(30,140,190))));

        public static final Color Neutral = new Color("Tunnel::Neutral", new Scalar(160,160,160));
    }



//    public static final Color purpleTrainColor = new Color("Train::Purple",
//            new ArrayList<>(Arrays.asList(
//                    new Scalar(170, 90, 110),
//                    new Scalar(100,30,60))));

    public static final Color purpleTrainColor = new Color("Train::Purple",
            new ArrayList<>(Arrays.asList(
                    new Scalar(170, 90, 110),
                    new Scalar(140, 85, 115),
                    new Scalar(150, 55, 85),
                    new Scalar(100, 60, 75),

                    new Scalar(100,30,60),
                    new Scalar(150,80,100),
                    new Scalar(140,85,115))));

//    public static Color pinkTrainColor = new Color("Train::Pink",new Scalar(160, 140, 220));

    public static final Color pinkTrainColor = new Color("Train::Pink",
            new ArrayList<>(Arrays.asList(
                    new Scalar(160, 140, 220),
                    new Scalar(100, 100, 185),
                    new Scalar(140, 120, 205),
                    new Scalar(140, 140, 220),
                    new Scalar(75, 75, 145),

                    new Scalar(170,140,220))));

}
