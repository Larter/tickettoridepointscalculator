package sosnicki.tickettoridepointscalculator.Game;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import sosnicki.tickettoridepointscalculator.Player.Ticket;

/**
 * Created by Marek Sośnicki on 2018-05-01.
 */

public class NordicCountriesScenario implements IScenario {

    List<RouteDetails> routes_ = new LinkedList<>();
    List<Ticket> tickets_ = new LinkedList<>();

    private class City {
        static public final String Honningsvag = "Honningsvag";
        static public final String Kirkens = "Kirkens";
        static public final String Murmansk = "Murmansk";
        static public final String Tromso = "Tromso";
        static public final String Rovaniemi = "Rovaniemi";
        static public final String Narvik = "Narvik";
        static public final String Kiruna = "Kiruna";
        static public final String Tornio = "Tornio";
        static public final String Boden = "Boden";
        static public final String Kajaani = "Kajaani";
        static public final String Lieksa = "Lieksa";
        static public final String Oulu = "Oulu";
        static public final String Umea = "Umea";
        static public final String Vaasa = "Vaasa";
        static public final String Imatra = "Imatra";
        static public final String Lahti = "Lahti";
        static public final String Tampere = "Tampere";
        static public final String Moirana = "Moirana";
        static public final String Ostersund = "Ostersund";
        static public final String Trondheim = "Trondheim";
        static public final String Sundsvall = "Sundsvall";
        static public final String Turku = "Turku";
        static public final String Kuopio = "Kuopio";
        static public final String Helsinki = "Helsinki";
        static public final String Andalsnes = "Andalsnes";
        static public final String Lillehammer = "Lillehammer";
        static public final String Orebro = "Orebro";
        static public final String Stockholm = "Stockholm";
        static public final String Tallinn = "Tallinn";
        static public final String Norrkoping = "Norrkoping";
        static public final String Oslo = "Oslo";
        static public final String Bergen = "Bergen";
        static public final String Stavanger = "Stavanger";
        static public final String Kristiansand = "Kristiansand";
        static public final String Alborg = "Alborg";
        static public final String Arhus = "Arhus";
        static public final String Goteborg = "Goteborg";
        static public final String Karlskrona = "Karlskrona";
        static public final String Kobenhavn = "Kobenhavn";
    }

    public NordicCountriesScenario() {
        fillBlackRoutes();
        fillNeutralRoutes();
        fillPurpleRoutes();
        fillOrangeRoutes();
        fillWhiteRoutes();
        fillYellowRoutes();
        fillGreenRoutes();
        fillRedRoutes();
        fillBlueRoutes();

        fillTickets();
    }

    private void fillTickets() {
        tickets_.add(new Ticket(City.Helsinki, City.Kobenhavn, 10));
        tickets_.add(new Ticket(City.Helsinki, City.Lieksa, 5));
        tickets_.add(new Ticket(City.Helsinki, City.Kirkens, 13));
        tickets_.add(new Ticket(City.Helsinki, City.Bergen, 12));
        tickets_.add(new Ticket(City.Helsinki, City.Kiruna, 10));
        tickets_.add(new Ticket(City.Helsinki, City.Ostersund, 8));


        tickets_.add(new Ticket(City.Oslo, City.Helsinki, 8));
        tickets_.add(new Ticket(City.Oslo, City.Stockholm, 4));
        tickets_.add(new Ticket(City.Oslo, City.Kobenhavn, 4));
        tickets_.add(new Ticket(City.Oslo, City.Honningsvag, 21));
        tickets_.add(new Ticket(City.Oslo, City.Moirana, 10));
        tickets_.add(new Ticket(City.Oslo, City.Vaasa, 9));
        tickets_.add(new Ticket(City.Oslo, City.Stavanger, 4));

        tickets_.add(new Ticket(City.Stockholm, City.Kobenhavn, 6));
        tickets_.add(new Ticket(City.Stockholm, City.Bergen, 8));
        tickets_.add(new Ticket(City.Stockholm, City.Kajaani, 10));
        tickets_.add(new Ticket(City.Stockholm, City.Imatra, 7));
        tickets_.add(new Ticket(City.Stockholm, City.Tromso, 17));
        tickets_.add(new Ticket(City.Stockholm, City.Umea, 7));

        tickets_.add(new Ticket(City.Goteborg, City.Andalsnes, 6));
        tickets_.add(new Ticket(City.Goteborg, City.Turku, 7));
        tickets_.add(new Ticket(City.Goteborg, City.Oulu, 12));

        tickets_.add(new Ticket(City.Bergen, City.Narvik, 16));
        tickets_.add(new Ticket(City.Bergen, City.Trondheim, 7));
        tickets_.add(new Ticket(City.Bergen, City.Kobenhavn, 8));
        tickets_.add(new Ticket(City.Bergen, City.Tornio, 17));

        tickets_.add(new Ticket(City.Kobenhavn, City.Oulu, 14));
        tickets_.add(new Ticket(City.Kobenhavn, City.Murmansk, 24));
        tickets_.add(new Ticket(City.Kobenhavn, City.Narvik, 18));

        tickets_.add(new Ticket(City.Narvik, City.Murmansk, 12));
        tickets_.add(new Ticket(City.Narvik, City.Tallinn, 13));

        tickets_.add(new Ticket(City.Alborg, City.Norrkoping, 5));
        tickets_.add(new Ticket(City.Alborg, City.Umea, 11));

        tickets_.add(new Ticket(City.Tampere, City.Tallinn, 3));
        tickets_.add(new Ticket(City.Tampere, City.Boden, 6));
        tickets_.add(new Ticket(City.Tampere, City.Kristiansand, 10));

        tickets_.add(new Ticket(City.Stavanger, City.Rovaniemi, 18));
        tickets_.add(new Ticket(City.Stavanger, City.Karlskrona, 8));

        tickets_.add(new Ticket(City.Tromso, City.Vaasa, 11));
        tickets_.add(new Ticket(City.Orebro, City.Kuopio, 10));
        tickets_.add(new Ticket(City.Norrkoping, City.Boden, 11));
        tickets_.add(new Ticket(City.Arhus, City.Lillehammer, 6));
        tickets_.add(new Ticket(City.Kristiansand, City.Moirana, 12));
        tickets_.add(new Ticket(City.Turku, City.Trondheim, 10));
        tickets_.add(new Ticket(City.Tornio, City.Imatra, 6));
        tickets_.add(new Ticket(City.Sundsvall, City.Lahti, 6));

    }

    @Override
    public List<RouteDetails> getRoutes() {
        return routes_;
    }

    @Override
    public List<Ticket> getTickets() {
        return tickets_;
    }


    private void fillBlueRoutes() {
        routes_.add(new RouteDetails(
                City.Kirkens,
                City.Rovaniemi,
                Colors.Route.Blue,
                Arrays.asList(
                        new RelativePosition(0.62, 0.09),
                        new RelativePosition(0.655, 0.115),
                        new RelativePosition(0.675, 0.15),
                        new RelativePosition(0.67, 0.187),
                        new RelativePosition(0.638, 0.22)
                )
        ));

        routes_.add(new RouteDetails(
                City.Kajaani,
                City.Lieksa,
                Colors.Route.Blue,
                Arrays.asList(
                        new RelativePosition(0.835, 0.335)
                )
        ));

        routes_.add(new RouteDetails(
                City.Sundsvall,
                City.Vaasa,
                Colors.Ferry.Blue,
                Arrays.asList(
                        new RelativePosition(0.555, 0.545),
                        new RelativePosition(0.60, 0.528),
                        new RelativePosition(0.62, 0.495)
                )
        ));

        routes_.add(new RouteDetails(
                City.Tampere,
                City.Lahti,
                Colors.Route.Blue,
                Arrays.asList(
                        new RelativePosition(0.79, 0.53)
                )
        ));


        routes_.add(new RouteDetails(
                City.Stockholm,
                City.Turku,
                Colors.Ferry.Blue,
                Arrays.asList(
                        new RelativePosition(0.65, 0.66),
                        new RelativePosition(0.671, 0.63),
                        new RelativePosition(0.71, 0.605)
                )
        ));


        routes_.add(new RouteDetails(
                City.Bergen,
                City.Oslo,
                Colors.Tunnel.Blue,
                Arrays.asList(
                        new RelativePosition(0.132, 0.745),
                        new RelativePosition(0.192, 0.73),
                        new RelativePosition(0.255, 0.73),
                        new RelativePosition(0.312, 0.747)
                )
        ));


        routes_.add(new RouteDetails(
                City.Goteborg,
                City.Orebro,
                Colors.Route.Blue,
                Arrays.asList(
                        new RelativePosition(0.472, 0.817),
                        new RelativePosition(0.489, 0.78)
                )
        ));

        routes_.add(new RouteDetails(
                City.Kobenhavn,
                City.Karlskrona,
                Colors.Ferry.Blue,
                Arrays.asList(
                        new RelativePosition(0.58, 0.945),
                        new RelativePosition(0.63, 0.93)
                )
        ));
    }

    private void fillRedRoutes() {
        routes_.add(new RouteDetails(
                City.Tornio,
                City.Rovaniemi,
                Colors.Route.Red,
                Arrays.asList(
                        new RelativePosition(0.595, 0.27)
                )
        ));

        routes_.add(new RouteDetails(
                City.Boden,
                City.Umea,
                Colors.Route.Red,
                Arrays.asList(
                        new RelativePosition(0.532, 0.335),
                        new RelativePosition(0.54, 0.37),
                        new RelativePosition(0.55, 0.402)
                )
        ));

        routes_.add(new RouteDetails(
                City.Moirana,
                City.Trondheim,
                Colors.Ferry.Red,
                Arrays.asList(
                        new RelativePosition(0.22, 0.385),
                        new RelativePosition(0.19, 0.41),
                        new RelativePosition(0.172, 0.45),
                        new RelativePosition(0.172, 0.483),
                        new RelativePosition(0.19, 0.52),
                        new RelativePosition(0.22, 0.546)
                )
        ));
        routes_.add(new RouteDetails(
                City.Tampere,
                City.Turku,
                Colors.Route.Red,
                Arrays.asList(
                        new RelativePosition(0.74, 0.56)
                )
        ));

        routes_.add(new RouteDetails(
                City.Helsinki,
                City.Imatra,
                Colors.Route.Red,
                Arrays.asList(
                        new RelativePosition(0.89, 0.565),
                        new RelativePosition(0.92, 0.535),
                        new RelativePosition(0.93, 0.50)
                )
        ));
        routes_.add(new RouteDetails(
                City.Bergen,
                City.Oslo,
                Colors.Tunnel.Red,
                Arrays.asList(
                        new RelativePosition(0.15, 0.76),
                        new RelativePosition(0.19, 0.745),
                        new RelativePosition(0.25, 0.745),
                        new RelativePosition(0.30, 0.76)
                )
        ));

        routes_.add(new RouteDetails(
                City.Norrkoping,
                City.Stockholm,
                Colors.Route.Red,
                Arrays.asList(
                        new RelativePosition(0.65, 0.74)
                )
        ));


        routes_.add(new RouteDetails(
                City.Kristiansand,
                City.Alborg,
                Colors.Ferry.Red,
                Arrays.asList(
                        new RelativePosition(0.31, 0.885),
                        new RelativePosition(0.35, 0.91)
                )
        ));
    }

    private void fillGreenRoutes() {
        routes_.add(new RouteDetails(
                City.Honningsvag,
                City.Kirkens,
                Colors.Ferry.Green,
                Arrays.asList(
                        new RelativePosition(0.495, 0.05),
                        new RelativePosition(0.54, 0.065)
                )
        ));

        routes_.add(new RouteDetails(
                City.Boden,
                City.Tornio,
                Colors.Route.Green,
                Arrays.asList(
                        new RelativePosition(0.56, 0.305)
                )
        ));

        routes_.add(new RouteDetails(
                City.Kajaani,
                City.Kuopio,
                Colors.Route.Green,
                Arrays.asList(
                        new RelativePosition(0.79, 0.35),
                        new RelativePosition(0.82, 0.385)
                )
        ));

        routes_.add(new RouteDetails(
                City.Moirana,
                City.Trondheim,
                Colors.Tunnel.Green,
                Arrays.asList(
                        new RelativePosition(0.26, 0.40),
                        new RelativePosition(0.26, 0.435),
                        new RelativePosition(0.26, 0.47),
                        new RelativePosition(0.26, 0.507),
                        new RelativePosition(0.26, 0.542)
                )
        ));

        routes_.add(new RouteDetails(
                City.Ostersund,
                City.Sundsvall,
                Colors.Route.Green,
                Arrays.asList(
                        new RelativePosition(0.41, 0.555),
                        new RelativePosition(0.46, 0.563)
                )
        ));


        routes_.add(new RouteDetails(
                City.Oslo,
                City.Orebro,
                Colors.Route.Green,
                Arrays.asList(
                        new RelativePosition(0.40, 0.763),
                        new RelativePosition(0.45, 0.75)
                )
        ));

        routes_.add(new RouteDetails(
                City.Stockholm,
                City.Tallinn,
                Colors.Ferry.Green,
                Arrays.asList(
                        new RelativePosition(0.69, 0.715),
                        new RelativePosition(0.75, 0.71),
                        new RelativePosition(0.792, 0.695),
                        new RelativePosition(0.83, 0.672)
                )
        ));

        routes_.add(new RouteDetails(
                City.Stavanger,
                City.Kristiansand,
                Colors.Tunnel.Green,
                Arrays.asList(
                        new RelativePosition(0.20, 0.85),
                        new RelativePosition(0.255, 0.85)
                )
        ));

        routes_.add(new RouteDetails(
                City.Kobenhavn,
                City.Karlskrona,
                Colors.Ferry.Green,
                Arrays.asList(
                        new RelativePosition(0.57, 0.93),
                        new RelativePosition(0.62, 0.917)
                )
        ));
    }

    private void fillYellowRoutes() {
        routes_.add(new RouteDetails(
                City.Narvik,
                City.Tromso,
                Colors.Ferry.Yellow,
                Arrays.asList(
                        new RelativePosition(0.25, 0.215),
                        new RelativePosition(0.235, 0.18),
                        new RelativePosition(0.27, 0.15)
                )
        ));

        routes_.add(new RouteDetails(
                City.Kajaani,
                City.Oulu,
                Colors.Route.Yellow,
                Arrays.asList(
                        new RelativePosition(0.70, 0.33),
                        new RelativePosition(0.755, 0.32)
                )
        ));


        routes_.add(new RouteDetails(
                City.Sundsvall,
                City.Umea,
                Colors.Route.Yellow,
                Arrays.asList(
                        new RelativePosition(0.52, 0.527),
                        new RelativePosition(0.53, 0.49),
                        new RelativePosition(0.54, 0.458)
                )
        ));

        routes_.add(new RouteDetails(
                City.Lahti,
                City.Imatra,
                Colors.Route.Yellow,
                Arrays.asList(
                        new RelativePosition(0.87, 0.51),
                        new RelativePosition(0.91, 0.485)
                )
        ));

        routes_.add(new RouteDetails(
                City.Andalsnes,
                City.Lillehammer,
                Colors.Tunnel.Yellow,
                Arrays.asList(
                        new RelativePosition(0.22, 0.64),
                        new RelativePosition(0.256, 0.665)
                )
        ));

        routes_.add(new RouteDetails(
                City.Stockholm,
                City.Helsinki,
                Colors.Ferry.Yellow,
                Arrays.asList(
                        new RelativePosition(0.67, 0.675),
                        new RelativePosition(0.714, 0.655),
                        new RelativePosition(0.754, 0.63),
                        new RelativePosition(0.796, 0.61)
                )
        ));


        routes_.add(new RouteDetails(
                City.Oslo,
                City.Orebro,
                Colors.Route.Yellow,
                Arrays.asList(
                        new RelativePosition(0.393, 0.75),
                        new RelativePosition(0.45, 0.735)
                )
        ));

        routes_.add(new RouteDetails(
                City.Norrkoping,
                City.Karlskrona,
                Colors.Route.Yellow,
                Arrays.asList(
                        new RelativePosition(0.65, 0.805),
                        new RelativePosition(0.66, 0.84),
                        new RelativePosition(0.67, 0.88)
                )
        ));
    }

    private void fillWhiteRoutes() {
        routes_.add(new RouteDetails(
                City.Tornio,
                City.Oulu,
                Colors.Route.White,
                Arrays.asList(
                        new RelativePosition(0.625, 0.32)
                )
        ));

        routes_.add(new RouteDetails(
                City.Kirkens,
                City.Murmansk,
                Colors.Ferry.White,
                Arrays.asList(
                        new RelativePosition(0.60, 0.05),
                        new RelativePosition(0.65, 0.035),
                        new RelativePosition(0.70, 0.04)
                )
        ));


        routes_.add(new RouteDetails(
                City.Narvik,
                City.Kiruna,
                Colors.Tunnel.White,
                Arrays.asList(
                        new RelativePosition(0.31, 0.25)
                )
        ));


        routes_.add(new RouteDetails(
                City.Boden,
                City.Umea,
                Colors.Route.White,
                Arrays.asList(
                        new RelativePosition(0.51, 0.335),
                        new RelativePosition(0.52, 0.37),
                        new RelativePosition(0.53, 0.405)
                )
        ));


        routes_.add(new RouteDetails(
                City.Kuopio,
                City.Lahti,
                Colors.Route.White,
                Arrays.asList(
                        new RelativePosition(0.85, 0.43),
                        new RelativePosition(0.83, 0.465),
                        new RelativePosition(0.82, 0.50)
                )
        ));


        routes_.add(new RouteDetails(
                City.Turku,
                City.Helsinki,
                Colors.Route.White,
                Arrays.asList(
                        new RelativePosition(0.79, 0.589)
                )
        ));


        routes_.add(new RouteDetails(
                City.Andalsnes,
                City.Trondheim,
                Colors.Ferry.White,
                Arrays.asList(
                        new RelativePosition(0.178, 0.59),
                        new RelativePosition(0.21, 0.57)
                )
        ));

        routes_.add(new RouteDetails(
                City.Oslo,
                City.Alborg,
                Colors.Ferry.White,
                Arrays.asList(
                        new RelativePosition(0.36, 0.81),
                        new RelativePosition(0.37, 0.845),
                        new RelativePosition(0.375, 0.88)
                )
        ));

        routes_.add(new RouteDetails(
                City.Norrkoping,
                City.Karlskrona,
                Colors.Route.White,
                Arrays.asList(
                        new RelativePosition(0.63, 0.805),
                        new RelativePosition(0.64, 0.842),
                        new RelativePosition(0.65, 0.88)
                )
        ));
    }

    private void fillOrangeRoutes() {
        routes_.add(new RouteDetails(
                City.Kiruna,
                City.Boden,
                Colors.Route.Orange,
                Arrays.asList(
                        new RelativePosition(0.38, 0.275),
                        new RelativePosition(0.42, 0.30),
                        new RelativePosition(0.47, 0.31)
                )
        ));


        routes_.add(new RouteDetails(
                City.Rovaniemi,
                City.Oulu,
                Colors.Route.Orange,
                Arrays.asList(
                        new RelativePosition(0.65, 0.30),
                        new RelativePosition(0.63, 0.27)
                )
        ));


        routes_.add(new RouteDetails(
                City.Moirana,
                City.Narvik,
                Colors.Ferry.Orange,
                Arrays.asList(
                        new RelativePosition(0.23, 0.35),
                        new RelativePosition(0.21, 0.315),
                        new RelativePosition(0.215, 0.28),
                        new RelativePosition(0.24, 0.25)
                )
        ));

        routes_.add(new RouteDetails(
                City.Sundsvall,
                City.Orebro,
                Colors.Route.Orange,
                Arrays.asList(
                        new RelativePosition(0.49, 0.59),
                        new RelativePosition(0.49, 0.623),
                        new RelativePosition(0.49, 0.66),
                        new RelativePosition(0.49, 0.698)
                )
        ));

        routes_.add(new RouteDetails(
                City.Tampere,
                City.Helsinki,
                Colors.Route.Orange,
                Arrays.asList(
                        new RelativePosition(0.79, 0.56)
                )
        ));

        routes_.add(new RouteDetails(
                City.Trondheim,
                City.Lillehammer,
                Colors.Tunnel.Orange,
                Arrays.asList(
                        new RelativePosition(0.28, 0.595),
                        new RelativePosition(0.31, 0.625),
                        new RelativePosition(0.31, 0.66)
                )
        ));

        routes_.add(new RouteDetails(
                City.Norrkoping,
                City.Stockholm,
                Colors.Route.Orange,
                Arrays.asList(
                        new RelativePosition(0.627, 0.74)
                )
        ));

        routes_.add(new RouteDetails(
                City.Oslo,
                City.Goteborg,
                Colors.Route.Orange,
                Arrays.asList(
                        new RelativePosition(0.39, 0.802),
                        new RelativePosition(0.43, 0.83)
                )
        ));


        routes_.add(new RouteDetails(
                City.Stavanger,
                City.Kristiansand,
                Colors.Route.Orange,
                Arrays.asList(
                        new RelativePosition(0.17, 0.875),
                        new RelativePosition(0.22, 0.89),
                        new RelativePosition(0.27, 0.878)
                )
        ));
    }

    private void fillPurpleRoutes() {
        routes_.add(new RouteDetails(
                City.Tromso,
                City.Honningsvag,
                Colors.Ferry.Purple,
                Arrays.asList(
                        new RelativePosition(0.33, 0.12),
                        new RelativePosition(0.35, 0.09),
                        new RelativePosition(0.38, 0.065),
                        new RelativePosition(0.43, 0.045)
                )
        ));

        routes_.add(new RouteDetails(
                City.Narvik,
                City.Kiruna,
                Colors.Tunnel.Purple,
                Arrays.asList(
                        new RelativePosition(0.32, 0.235)
                )
        ));

        routes_.add(new RouteDetails(
                City.Kuopio,
                City.Imatra,
                Colors.Route.Purple,
                Arrays.asList(
                        new RelativePosition(0.89, 0.412),
                        new RelativePosition(0.93, 0.44)
                )
        ));

        routes_.add(new RouteDetails(
                City.Sundsvall,
                City.Umea,
                Colors.Route.Purple,
                Arrays.asList(
                        new RelativePosition(0.50, 0.52),
                        new RelativePosition(0.51, 0.49),
                        new RelativePosition(0.52, 0.455)
                )
        ));


        routes_.add(new RouteDetails(
                City.Vaasa,
                City.Tampere,
                Colors.Route.Purple,
                Arrays.asList(
                        new RelativePosition(0.67, 0.49),
                        new RelativePosition(0.71, 0.51)
                )
        ));

        routes_.add(new RouteDetails(
                City.Lillehammer,
                City.Oslo,
                Colors.Tunnel.Purple,
                Arrays.asList(
                        new RelativePosition(0.32, 0.70),
                        new RelativePosition(0.35, 0.735)
                )
        ));

        routes_.add(new RouteDetails(
                City.Orebro,
                City.Stockholm,
                Colors.Route.Purple,
                Arrays.asList(
                        new RelativePosition(0.53, 0.715),
                        new RelativePosition(0.575, 0.705)
                )
        ));


        routes_.add(new RouteDetails(
                City.Helsinki,
                City.Tallinn,
                Colors.Ferry.Purple,
                Arrays.asList(
                        new RelativePosition(0.88, 0.61),
                        new RelativePosition(0.895, 0.635)

                )
        ));


        routes_.add(new RouteDetails(
                City.Bergen,
                City.Stavanger,
                Colors.Ferry.Purple,
                Arrays.asList(
                        new RelativePosition(0.10, 0.80),
                        new RelativePosition(0.12, 0.835)

                )
        ));


        routes_.add(new RouteDetails(
                City.Alborg,
                City.Arhus,
                Colors.Route.Purple,
                Arrays.asList(
                        new RelativePosition(0.41, 0.935)
                )
        ));
    }


    private void fillNeutralRoutes() {
        routes_.add(new RouteDetails(
                City.Murmansk,
                City.Lieksa,
                Colors.Route.Neutral,
                Arrays.asList(
                        new RelativePosition(0.775, 0.06),
                        new RelativePosition(0.82, 0.085),
                        new RelativePosition(0.855, 0.11),
                        new RelativePosition(0.89, 0.14),
                        new RelativePosition(0.91, 0.175),
                        new RelativePosition(0.92, 0.215),
                        new RelativePosition(0.92, 0.25),
                        new RelativePosition(0.915, 0.285),
                        new RelativePosition(0.90, 0.32)
                )
        ));


        routes_.add(new RouteDetails(
                City.Oulu,
                City.Kuopio,
                Colors.Route.Neutral,
                Arrays.asList(
                        new RelativePosition(0.695, 0.355),
                        new RelativePosition(0.74, 0.375),
                        new RelativePosition(0.79, 0.395)
                )
        ));

        routes_.add(new RouteDetails(
                City.Umea,
                City.Vaasa,
                Colors.Ferry.Neutral,
                Arrays.asList(
                        new RelativePosition(0.58, 0.445)
                )
        ));

        routes_.add(new RouteDetails(
                City.Vaasa,
                City.Kuopio,
                Colors.Route.Neutral,
                Arrays.asList(
                        new RelativePosition(0.66, 0.455),
                        new RelativePosition(0.71, 0.445),
                        new RelativePosition(0.76, 0.43),
                        new RelativePosition(0.81, 0.415)
                )
        ));

        routes_.add(new RouteDetails(
                City.Sundsvall,
                City.Stockholm,
                Colors.Route.Neutral,
                Arrays.asList(
                        new RelativePosition(0.52, 0.58),
                        new RelativePosition(0.54, 0.61),
                        new RelativePosition(0.57, 0.64),
                        new RelativePosition(0.595, 0.67)
                )
        ));


        routes_.add(new RouteDetails(
                City.Sundsvall,
                City.Stockholm,
                Colors.Route.Neutral,
                Arrays.asList(
                        new RelativePosition(0.535, 0.57),
                        new RelativePosition(0.56, 0.60),
                        new RelativePosition(0.59, 0.63),
                        new RelativePosition(0.615, 0.665)
                )
        ));


        routes_.add(new RouteDetails(
                City.Stockholm,
                City.Helsinki,
                Colors.Ferry.Neutral,
                Arrays.asList(
                        new RelativePosition(0.69, 0.69),
                        new RelativePosition(0.73, 0.665),
                        new RelativePosition(0.77, 0.642),
                        new RelativePosition(0.81, 0.62)
                )
        ));

        routes_.add(new RouteDetails(
                City.Orebro,
                City.Norrkoping,
                Colors.Route.Neutral,
                Arrays.asList(
                        new RelativePosition(0.53, 0.76),
                        new RelativePosition(0.58, 0.765)
                )
        ));


        routes_.add(new RouteDetails(
                City.Goteborg,
                City.Norrkoping,
                Colors.Route.Neutral,
                Arrays.asList(
                        new RelativePosition(0.50, 0.835),
                        new RelativePosition(0.55, 0.81),
                        new RelativePosition(0.59, 0.79)
                )
        ));

        routes_.add(new RouteDetails(
                City.Alborg,
                City.Goteborg,
                Colors.Ferry.Neutral,
                Arrays.asList(
                        new RelativePosition(0.43, 0.91),
                        new RelativePosition(0.455, 0.885)
                )
        ));

        routes_.add(new RouteDetails(
                City.Arhus,
                City.Kobenhavn,
                Colors.Ferry.Neutral,
                Arrays.asList(
                        new RelativePosition(0.475, 0.955)
                )
        ));
    }

    private void fillBlackRoutes() {
        routes_.add(new RouteDetails(
                City.Kiruna,
                City.Boden,
                Colors.Route.Black,
                Arrays.asList(
                        new RelativePosition(0.39, 0.26),
                        new RelativePosition(0.43, 0.285),
                        new RelativePosition(0.49, 0.293)
                )
        ));

        routes_.add(new RouteDetails(
                City.Vaasa,
                City.Oulu,
                Colors.Route.Black,
                Arrays.asList(
                        new RelativePosition(0.63, 0.44),
                        new RelativePosition(0.64, 0.405),
                        new RelativePosition(0.65, 0.37)
                )
        ));

        routes_.add(new RouteDetails(
                City.Lieksa,
                City.Kuopio,
                Colors.Route.Black,
                Arrays.asList(
                        new RelativePosition(0.86, 0.375)
                )
        ));

        routes_.add(new RouteDetails(
                City.Trondheim,
                City.Ostersund,
                Colors.Tunnel.Black,
                Arrays.asList(
                        new RelativePosition(0.295, 0.57),
                        new RelativePosition(0.35, 0.554)
                )
        ));


        routes_.add(new RouteDetails(
                City.Lahti,
                City.Bergen,
                Colors.Route.Black,
                Arrays.asList(
                        new RelativePosition(0.84, 0.555)
                )
        ));

        routes_.add(new RouteDetails(
                City.Andalsnes,
                City.Ostersund,
                Colors.Ferry.Black,
                Arrays.asList(
                        new RelativePosition(0.14, 0.615),
                        new RelativePosition(0.095, 0.635),
                        new RelativePosition(0.07, 0.665),
                        new RelativePosition(0.06, 0.70),
                        new RelativePosition(0.075, 0.74)
                )
        ));

        routes_.add(new RouteDetails(
                City.Orebro,
                City.Stockholm,
                Colors.Route.Black,
                Arrays.asList(
                        new RelativePosition(0.545, 0.73),
                        new RelativePosition(0.59, 0.72)
                )
        ));


        routes_.add(new RouteDetails(
                City.Oslo,
                City.Kristiansand,
                Colors.Route.Black,
                Arrays.asList(
                        new RelativePosition(0.32, 0.79),
                        new RelativePosition(0.30, 0.83)
                )
        ));

        routes_.add(new RouteDetails(
                City.Goteborg,
                City.Kobenhavn,
                Colors.Ferry.Black,
                Arrays.asList(
                        new RelativePosition(0.48, 0.885),
                        new RelativePosition(0.505, 0.92)
                )
        ));
    }

}
