package sosnicki.tickettoridepointscalculator;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sosnicki.tickettoridepointscalculator.Game.Game;
import sosnicki.tickettoridepointscalculator.Game.GameConfiguration;
import sosnicki.tickettoridepointscalculator.Player.Player;
import sosnicki.tickettoridepointscalculator.Player.Ticket;

public class AddTicket extends AppCompatActivity {


    Player player_;

    Spinner fromSpinner_;
    Spinner toSpinner_;


    TextView titleTextView_;
    TextView pointsTextView_;

    Button addButton_;
    Button removeButton_;
    Button finishButton_;

    ListView ticketsView_;
    ArrayAdapter<String> fromAdapter_;
    ArrayAdapter<String> toAdapter_;
    List<String> possibleFrom_;
    List<String> possibleTo_;
    Map<String, Set<String>> possibleTickets_;

    List<String> currentTickets_;
    ArrayAdapter<String> ticketAdapter_;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ticket);


        fromSpinner_ = findViewById(R.id.SpinnerFrom);
        toSpinner_ = findViewById(R.id.SpinnerTo);
        titleTextView_ = findViewById(R.id.TitleTextView);
        pointsTextView_ = findViewById(R.id.PointsTextView);
        addButton_ = findViewById(R.id.ButtonAdd);
        removeButton_ = findViewById(R.id.ButtonRemove);
        finishButton_ = findViewById(R.id.ButtonExit);
        ticketsView_ = findViewById(R.id.ticketsListView);

        String playerName = getIntent().getExtras().getString("Player");
        if (playerName != null) {
            titleTextView_.setText(String.format("Tickets for : %s", playerName));
            player_ = CurrentGameData.currentGame.getPlayer(playerName);
            titleTextView_.setBackgroundColor(player_.getColor().getHashCode());
        }
        else {
            finish();
        }

        possibleTickets_ = CurrentGameData.currentGame.getPossibleTickets();


        possibleTo_ = new ArrayList<>();

        toAdapter_ = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, possibleTo_);

        toAdapter_.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        possibleFrom_ = new ArrayList<>();
        possibleFrom_.addAll(possibleTickets_.keySet());
        fromAdapter_ =
                new ArrayAdapter<>(this,
                        android.R.layout.simple_spinner_item, possibleFrom_);
        fromAdapter_.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        fromSpinner_.setAdapter(fromAdapter_);
        toSpinner_.setAdapter(toAdapter_);


        fromSpinner_.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String currentFrom = fromSpinner_.getSelectedItem().toString();
                possibleTo_.clear();
                toAdapter_.clear();
                toAdapter_.addAll(possibleTickets_.get(currentFrom));
                toSpinner_.setSelection(0, true);

                updatePointsValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        toSpinner_.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                updatePointsValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                updatePointsValue();
            }
        });


        finishButton_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        addButton_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTicket();
            }
        });

        removeButton_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeTicket();
            }
        });


        currentTickets_ = new ArrayList<>();
        ticketAdapter_ = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, currentTickets_);
        ticketsView_.setAdapter(ticketAdapter_);
        updateTickets();

        ticketsView_.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                for (Ticket ticket : player_.getTickets())
                {
                    if(ticketAdapter_.getItem(i).contentEquals(ticket.toString()))
                    {
                        for(int fromId = 0; fromId< possibleFrom_.size(); ++fromId)
                        {
                            if(possibleFrom_.get(fromId).contentEquals(ticket.getFrom()))
                            {
                                fromSpinner_.setSelection(fromId);
                            }
                        }

                        for (int toId = 0; toId<possibleTo_.size(); ++toId)
                        {
                            if(possibleTo_.get(toId).contentEquals(ticket.getTo()))
                            {
                                toSpinner_.setSelection(toId);
                            }
                        }
                        break;
                    }
                }
            }
        });

    }

    private void updateTickets() {
        ticketAdapter_.clear();
        for(Ticket ticket: player_.getTickets())
        {
            ticketAdapter_.add(ticket.toString());
        }
    }

    private void updatePointsValue() {
        String currentFrom = fromSpinner_.getSelectedItem().toString();
        String currentTo = toSpinner_.getSelectedItem().toString();

        Ticket ticket = CurrentGameData.currentGame.getTicket(currentFrom, currentTo);
        if (ticket != null) {
            pointsTextView_.setText(String.format("Points: %d", ticket.getValue()));
        } else {
            pointsTextView_.setText("Uknown connection? f:" + currentFrom + " t:" + currentTo);
        }
    }

    private void addTicket() {
        String currentFrom = fromSpinner_.getSelectedItem().toString();
        String currentTo = toSpinner_.getSelectedItem().toString();

        Ticket ticket = CurrentGameData.currentGame.getTicket(currentFrom, currentTo);

        if (ticket != null) {
            player_.addTicket(ticket);
            updateTickets();
        }
    }

    private void removeTicket(){
        String currentFrom = fromSpinner_.getSelectedItem().toString();
        String currentTo = toSpinner_.getSelectedItem().toString();

        Ticket ticket = CurrentGameData.currentGame.getTicket(currentFrom, currentTo);

        if (ticket != null) {
            player_.removeTicket(ticket);
            updateTickets();
        }

    }


    private void showToast(String message) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }


    @Override
    public void finish() {
        // Prepare data intent
        Intent data = new Intent();
//            data.putExtra("returnKey1", "Swinging on a star. ");
//            data.putExtra("returnKey2", "You could be better then you are. ");
        // Activity finished ok, return the data
        setResult(RESULT_OK, data);
        super.finish();
    }

}
