package sosnicki.tickettoridepointscalculator;

import org.junit.Before;
import org.junit.Test;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.Arrays;
import java.util.List;

import sosnicki.tickettoridepointscalculator.Game.Color;
import sosnicki.tickettoridepointscalculator.Game.Colors;
import sosnicki.tickettoridepointscalculator.Game.Game;
import sosnicki.tickettoridepointscalculator.Game.GameConfiguration;
import sosnicki.tickettoridepointscalculator.Game.NordicCountriesScenario;
import sosnicki.tickettoridepointscalculator.ImageProcessing.RouteProcessor;
import sosnicki.tickettoridepointscalculator.Player.PointsResult;

import static org.junit.Assert.assertEquals;

/**
 * Created by Marek Sośnicki on 2018-05-05.
 */

public class GameTest {

    @Before
    public void initOpenCV() {
        String projectPath = System.getProperty("user.dir");
        String opencvpath = projectPath + "/app/libs/win/";
        System.load(opencvpath + Core.NATIVE_LIBRARY_NAME + ".dll");
    }

    Mat imageRead(String filename) {
        String p = getClass().getResource(filename).getPath();
        p = p.replaceFirst("/", "");
        Mat image = Imgcodecs.imread(p);


        int divider = 2;


        Mat result = new Mat(image.rows() / divider, image.cols() / divider, image.type());

        Imgproc.resize(image, result, result.size(), 0, 0, Imgproc.INTER_CUBIC);

        return result;

    }


    @Test
    public void fullTestWithTwoPlayers_1() {


        RouteProcessor processor = new RouteProcessor
                (imageRead("bottom_left.jpg"), imageRead("bottom_right.jpg"),
                        imageRead("top_left.jpg"), imageRead("top_right.jpg"));

        NordicCountriesScenario scenario = new NordicCountriesScenario();

        GameConfiguration configuration =
                new GameConfiguration(Arrays.asList(Colors.pinkTrainColor,
                                                    Colors.purpleTrainColor), scenario);

        Game game = new Game(configuration, processor);

        game.loadPointsFromImage(imageRead("two_color_trains.jpg"));

        PointsResult pinkResult = game.getResult(Colors.pinkTrainColor);

        PointsResult purpleResult = game.getResult(Colors.purpleTrainColor);
//        Imgcodecs.imwrite("E:\\fullTestTwoPlayersResult.jpg", processor.getImage());

        assertEquals(60, pinkResult.getPointsFromRoutes());
        assertEquals(50, purpleResult.getPointsFromRoutes());
    }

    @Test
    public void fullTestWithTwoPlayers_2() {


        RouteProcessor processor = new RouteProcessor
                (imageRead("bottom_left.jpg"), imageRead("bottom_right.jpg"),
                        imageRead("top_left.jpg"), imageRead("top_right.jpg"));

        NordicCountriesScenario scenario = new NordicCountriesScenario();

        GameConfiguration configuration =
                new GameConfiguration(Arrays.asList(Colors.pinkTrainColor,
                        Colors.purpleTrainColor), scenario);

        Game game = new Game(configuration, processor);

        game.loadPointsFromImage(imageRead("with_many_trains.jpg"));

        PointsResult pinkResult = game.getResult(Colors.pinkTrainColor);
        PointsResult purpleResult = game.getResult(Colors.purpleTrainColor);


        assertEquals(58, pinkResult.getPointsFromRoutes());
        assertEquals(57, purpleResult.getPointsFromRoutes());


    }

    @Test
    public void fullTestWithTwoPlayers_3() {


        RouteProcessor processor = new RouteProcessor
                (imageRead("bottom_left.jpg"), imageRead("bottom_right.jpg"),
                        imageRead("top_left.jpg"), imageRead("top_right.jpg"));

        NordicCountriesScenario scenario = new NordicCountriesScenario();

        GameConfiguration configuration =
                new GameConfiguration(Arrays.asList(Colors.pinkTrainColor,
                        Colors.purpleTrainColor), scenario);

        Game game = new Game(configuration, processor);

        game.loadPointsFromImage(imageRead("otherExampleTrains.jpg"));

        PointsResult pinkResult = game.getResult(Colors.pinkTrainColor);
        PointsResult purpleResult = game.getResult(Colors.purpleTrainColor);

//        Imgcodecs.imwrite("E:\\otherExampleResult.jpg", processor.getImage());

        assertEquals(61, pinkResult.getPointsFromRoutes());
        assertEquals(51, purpleResult.getPointsFromRoutes());
    }


    @Test
    public void twoGamesTest() {


        RouteProcessor processor = new RouteProcessor
                (imageRead("bottom_left.jpg"), imageRead("bottom_right.jpg"),
                        imageRead("top_left.jpg"), imageRead("top_right.jpg"));

        NordicCountriesScenario scenario = new NordicCountriesScenario();

        GameConfiguration configuration =
                new GameConfiguration(Arrays.asList(Colors.pinkTrainColor,
                        Colors.purpleTrainColor), scenario);

        Game game = new Game(configuration, processor);

        game.loadPointsFromImage(imageRead("otherExampleTrains.jpg"));
        PointsResult pinkResult = game.getResult(Colors.pinkTrainColor);
        PointsResult purpleResult = game.getResult(Colors.purpleTrainColor);
        assertEquals(61, pinkResult.getPointsFromRoutes());
        assertEquals(51, purpleResult.getPointsFromRoutes());


        game.loadPointsFromImage(imageRead("with_many_trains.jpg"));

        pinkResult = game.getResult(Colors.pinkTrainColor);
        purpleResult = game.getResult(Colors.purpleTrainColor);
        assertEquals(58, pinkResult.getPointsFromRoutes());
        assertEquals(57, purpleResult.getPointsFromRoutes());


    }


}
