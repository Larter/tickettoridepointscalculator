package sosnicki.tickettoridepointscalculator;

/**
 * Created by Marek Sośnicki on 2018-04-30.
 */


import org.junit.Test;

import sosnicki.tickettoridepointscalculator.Game.Colors;
import sosnicki.tickettoridepointscalculator.Player.Player;
import sosnicki.tickettoridepointscalculator.Player.PointsResult;
import sosnicki.tickettoridepointscalculator.Player.Route;
import sosnicki.tickettoridepointscalculator.Player.Ticket;

import static org.junit.Assert.*;

public class PlayerTest {
    @Test
    public void ReturnColor(){
        Player player = new Player(Colors.purpleTrainColor, "dummy");

        assertEquals(Colors.purpleTrainColor, player.getColor());
    }

    @Test
    public void CalculateRoutesPointsValuesIfNoRoutesAndTicketsAreGiven()
    {
        Player player = new Player(Colors.purpleTrainColor, "dummy");

        PointsResult result = player.GetResult();

        assertEquals(0, result.getPointsFromRoutes());
        assertEquals(0, result.getPointsFromTickets());
    }

    @Test
    public void AddAndCalculatePointsFromSingleRoute()
    {
        Player player = new Player(Colors.purpleTrainColor, "dummy");
        player.addRoute(new Route("A", "B", 1));
        PointsResult result = player.GetResult();
        assertEquals(1, result.getPointsFromRoutes());
        assertEquals(0, result.getPointsFromTickets());
    }

    @Test
    public void AddAndCalculatePointsFromMultipleRoutes()
    {
        Player player = new Player(Colors.purpleTrainColor, "dummy");
        player.addRoute(new Route("A", "B", 1));// 1
        player.addRoute(new Route("A", "B", 2));// 2
        player.addRoute(new Route("A", "B", 3));// 4
        player.addRoute(new Route("A", "B", 4));// 7
        player.addRoute(new Route("A", "B", 5));// 10
        player.addRoute(new Route("A", "B", 6));// 15
        player.addRoute(new Route("A", "B", 9));// 27

        PointsResult result = player.GetResult();
        assertEquals(66, result.getPointsFromRoutes());
        assertEquals(0, result.getPointsFromTickets());
    }


    @Test
    public void AddAndCalculatePointsForSingleFinishedTicket()
    {
        Player player = new Player(Colors.purpleTrainColor, "dummy");
        player.addRoute(new Route("A", "B", 1));
        player.addTicket(new Ticket("A", "B", 10));
        PointsResult result = player.GetResult();
        assertEquals(1, result.getPointsFromRoutes());
        assertEquals(10, result.getPointsFromTickets());
    }

    @Test
    public void AddAndCalculatePointsForSingleUnfinishedTicket()
    {
        Player player = new Player(Colors.purpleTrainColor, "dummy");
        player.addRoute(new Route("A", "B", 1));
        player.addTicket(new Ticket("A", "C", 10));
        PointsResult result = player.GetResult();
        assertEquals(1, result.getPointsFromRoutes());
        assertEquals(-10, result.getPointsFromTickets());
    }

    @Test
    public void AddAndCalculatePointsForSingleFinishedTicketWithLongerRoute()
    {
        Player player = new Player(Colors.purpleTrainColor, "dummy");
        player.addRoute(new Route("A", "B", 1));
        player.addRoute(new Route("C", "B", 1));
        player.addRoute(new Route("C", "D", 1));
        player.addTicket(new Ticket("A", "D", 10));
        PointsResult result = player.GetResult();
        assertEquals(3, result.getPointsFromRoutes());
        assertEquals(10, result.getPointsFromTickets());
    }


    @Test
    public void CorrectlyCalculateMultipleTicketsAndRoutes()
    {
        Player player = new Player(Colors.purpleTrainColor, "dummy");
        player.addRoute(new Route("A", "B", 1));
        player.addRoute(new Route("C", "B", 1));
        player.addRoute(new Route("C", "D", 1));
        player.addRoute(new Route("E", "F", 1));
        player.addRoute(new Route("E", "G", 5));

        player.addTicket(new Ticket("A", "D", 3));
        player.addTicket(new Ticket("A", "F", 10));
        player.addTicket(new Ticket("F", "G", 5));

        PointsResult result = player.GetResult();
        assertEquals(14, result.getPointsFromRoutes());
        assertEquals(-2, result.getPointsFromTickets());
    }

}
