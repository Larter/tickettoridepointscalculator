package sosnicki.tickettoridepointscalculator;

import org.junit.Before;
import org.junit.Test;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import sosnicki.tickettoridepointscalculator.Game.Color;
import sosnicki.tickettoridepointscalculator.Game.Colors;
import sosnicki.tickettoridepointscalculator.Game.NordicCountriesScenario;
import sosnicki.tickettoridepointscalculator.Game.RelativePosition;
import sosnicki.tickettoridepointscalculator.Game.RouteDetails;
import sosnicki.tickettoridepointscalculator.ImageProcessing.RouteProcessor;
import sosnicki.tickettoridepointscalculator.Player.Player;
import sosnicki.tickettoridepointscalculator.Player.Route;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Marek Sośnicki on 2018-05-01.
 */

public class RouteProcessorTest {


    @Before
    public void initOpenCV() {
        String projectPath = System.getProperty("user.dir");
        String opencvpath = projectPath + "/app/libs/win/";
        System.load(opencvpath + Core.NATIVE_LIBRARY_NAME + ".dll");
    }


    Mat imageRead(String filename) {
        String p = getClass().getResource(filename).getPath();
        p = p.replaceFirst("/", "");
        Mat image =  Imgcodecs.imread(p);


        int divider = 2;


        Mat result = new Mat(image.rows()/divider, image.cols()/divider, image.type());

        Imgproc.resize(image, result, result.size(), 0,0, Imgproc.INTER_CUBIC);

        return result;

    }

//    @Test
//    public void LoadAndDisplayImage() {
//
//
//        List<Player> players = new ArrayList<Player>(Arrays.asList(new Player(Colors.purpleTrainColor)));
//
//        RouteProcessor processor = new RouteProcessor
//                (imageRead("basic_picture.jpg"),
//                        imageRead("bottom_left.jpg"), imageRead("bottom_right.jpg"),
//                        imageRead("top_left.jpg"), imageRead("top_right.jpg"), players);
//
//
//        RelativePosition relativePosition = new RelativePosition(0.755, 0.55);
//
//        RouteDetails details = new RouteDetails("A", "B", Colors.Route.Red,
//                new ArrayList<RelativePosition>(
//                Arrays.asList(relativePosition)));
//
////        processor.markPosition(relativePosition, new Scalar(0, 255, 0));
//
//        Player player = processor.checkPlayer(details);
//
//
//        Imgcodecs.imwrite("E:\\result.jpg", processor.getCornersMarked());
//        assertEquals(null, player);
//
//    }


//    @Test
//    public void DrawRelativePositions() {
//
//
//        List<Player> players = new ArrayList<Player>(Arrays.asList(new Player(Colors.purpleTrainColor)));
//
//        RouteProcessor processor = new RouteProcessor
//                (imageRead("basic_picture.jpg"),
//                        imageRead("bottom_left.jpg"), imageRead("bottom_right.jpg"),
//                        imageRead("top_left.jpg"), imageRead("top_right.jpg"), players);
//
//
//        RelativePosition relativePosition = new RelativePosition(0.755, 0.55);
//
//        RouteDetails details = new RouteDetails("A", "B", Colors.Route.Red,
//                new ArrayList<RelativePosition>(
//                        Arrays.asList(relativePosition)));
//
////        processor.markPosition(relativePosition, new Scalar(0, 255, 0));
//
////        Player player = processor.checkPlayer(details);
//
//
//        for(double x = 0.01; x< 1; x+=0.02)
//        {
//            for (double y = 0.01; y<1; y+=0.01 )
//            {
//                relativePosition = new RelativePosition(x,y);
//                processor.markPosition(relativePosition, new Color("", new Scalar(255,255,255)));
//            }
//        }
//
//        Imgcodecs.imwrite("E:\\relative.jpg", processor.getCornersMarked());
////        assertEquals(null, player);
//
//    }


    @Test
    public void DrawAllRoutesNordicCountriesBasic() {
        NordicCountriesScenario scenario = new NordicCountriesScenario();

        List<Player> players = new ArrayList<>(Arrays.asList(
                new Player(Colors.purpleTrainColor),
                new Player(Colors.pinkTrainColor)));

        RouteProcessor processor = new RouteProcessor
                (       imageRead("bottom_left.jpg"), imageRead("bottom_right.jpg"),
                        imageRead("top_left.jpg"), imageRead("top_right.jpg"));
        processor.loadImage(imageRead("basic_picture.jpg"), players);

        for(RouteDetails route : scenario.getRoutes())
        {
            Player player = processor.checkPlayer(route);
            if(player!=null)
            {
                System.out.println("from: " + route.getFrom()
                        + " to: " + route.getTo()
                        + " routeColor: "+  route.getColor().toString()
                        + " playerColor: "+ player.getColor().toString());
            }
            assertEquals(player, null);
        }
//        Imgcodecs.imwrite("E:\\nordicResult.jpg", processor.getCornersMarked());
    }



    @Test
    public void DrawAllRoutesSingleColorTrain() {
        NordicCountriesScenario scenario = new NordicCountriesScenario();

        List<Player> players = new ArrayList<>(Arrays.asList(
                new Player(Colors.purpleTrainColor)));


        RouteProcessor processor = new RouteProcessor
                (       imageRead("bottom_left.jpg"), imageRead("bottom_right.jpg"),
                        imageRead("top_left.jpg"), imageRead("top_right.jpg"));
        processor.loadImage(imageRead("one_color_trains.jpg"), players);


        for(RouteDetails route : scenario.getRoutes())
        {
            Player player = processor.checkPlayer(route);
            if(player!=null)
            {
                System.out.println("from: " + route.getFrom()
                        + " to: " + route.getTo()
                        + " routeColor: "+  route.getColor().toString()
                        + " playerColor: "+ player.getColor().toString());
            }
//            assertEquals(player, null);
        }
        Imgcodecs.imwrite("E:\\nordicResult.jpg", processor.getImage());
    }

    @Test
    public void DrawAllRoutesSingleTwoColorTrains() {
        NordicCountriesScenario scenario = new NordicCountriesScenario();

        List<Player> players = new ArrayList<>(Arrays.asList(
                new Player(Colors.pinkTrainColor),
                new Player(Colors.purpleTrainColor)));

        RouteProcessor processor = new RouteProcessor
                (       imageRead("bottom_left.jpg"), imageRead("bottom_right.jpg"),
                        imageRead("top_left.jpg"), imageRead("top_right.jpg"));
        processor.loadImage(imageRead("two_color_trains.jpg"), players);

        for(RouteDetails route : scenario.getRoutes())
        {
            Player player = processor.checkPlayer(route);
            if(player!=null)
            {
                System.out.println("from: " + route.getFrom()
                        + " to: " + route.getTo()
                        + " routeColor: "+  route.getColor().toString()
                        + " playerColor: "+ player.getColor().toString());
            }
//            assertEquals(player, null);
        }
        Imgcodecs.imwrite("E:\\nordicResult_twoColorTrains.jpg", processor.getImage());
    }





    private class TInst
    {
        public String endpoint1_;
        public String endpoint2_;
        private Color playerColor_;

        TInst(String endpoint1, String endpoint2, Color playerColor)
        {
            endpoint1_ = endpoint1;
            endpoint2_ = endpoint2;
            playerColor_ = playerColor;
        }

        boolean isMatchingRoute(RouteDetails details)
        {
            return (details.getTo().equals(endpoint1_) && details.getFrom().equals(endpoint2_))
                    || (details.getTo().equals(endpoint2_) && details.getFrom().equals(endpoint1_));
        }

        Color getColor()
        {
            return playerColor_;
        }

    }

    @Test
    public void DrawAllRoutesNordicCountriesWithManyTrains() {
        NordicCountriesScenario scenario = new NordicCountriesScenario();

        List<Player> players = new ArrayList<>(Arrays.asList(
                new Player(Colors.purpleTrainColor),
                new Player(Colors.pinkTrainColor)));


        RouteProcessor processor = new RouteProcessor
                (       imageRead("bottom_left.jpg"), imageRead("bottom_right.jpg"),
                        imageRead("top_left.jpg"), imageRead("top_right.jpg"));
        processor.loadImage(imageRead("with_many_trains.jpg"), players);

        List<TInst> instances = new ArrayList<>(Arrays.asList(
                new TInst("Narvik", "Kiruna", Colors.purpleTrainColor),
                new TInst("Tornio", "Oulu", Colors.purpleTrainColor),
                new TInst("Kiruna", "Boden", Colors.purpleTrainColor),
                new TInst("Oulu", "Kajaani", Colors.purpleTrainColor),
                new TInst("Kajaani", "Lieksa", Colors.purpleTrainColor),
                new TInst("Vaasa", "Oulu", Colors.purpleTrainColor),
                new TInst("Moirana", "Narvik", Colors.purpleTrainColor),
                new TInst("Moirana", "Trondheim", Colors.purpleTrainColor),
                new TInst("Trondheim", "Ostersund", Colors.purpleTrainColor),
                new TInst("Ostersund", "Sundsvall", Colors.purpleTrainColor),
                new TInst("Sundsvall", "Stockholm", Colors.purpleTrainColor),
                new TInst("Stockholm", "Helsinki", Colors.purpleTrainColor),
                new TInst("Stockholm", "Norrkoping", Colors.purpleTrainColor),
                new TInst("Norrkoping", "Karlskrona", Colors.purpleTrainColor),
                new TInst("Karlskrona", "Kobenhavn", Colors.purpleTrainColor),
                new TInst("Kobenhavn", "Goteborg", Colors.purpleTrainColor),

                new TInst("Honningsvag", "Kirkens", Colors.pinkTrainColor),
                new TInst("Tromso", "Honningsvag", Colors.pinkTrainColor),
                new TInst("Kirkens", "Murmansk", Colors.pinkTrainColor),
                new TInst("Kirkens", "Rovaniemi", Colors.pinkTrainColor),
                new TInst("Tornio", "Rovaniemi", Colors.pinkTrainColor),
                new TInst("Tromso", "Narvik", Colors.pinkTrainColor),
                new TInst("Boden", "Tornio", Colors.pinkTrainColor),
                new TInst("Boden", "Umea", Colors.pinkTrainColor),
                new TInst("Vaasa", "Umea", Colors.pinkTrainColor),
                new TInst("Vaasa", "Tampere", Colors.pinkTrainColor),
                new TInst("Umea", "Sundsvall", Colors.pinkTrainColor),
                new TInst("Sundsvall", "Orebro", Colors.pinkTrainColor),
                new TInst("Orebro", "Stockholm", Colors.pinkTrainColor),
                new TInst("Orebro", "Oslo", Colors.pinkTrainColor),
                new TInst("Oslo", "Bergen", Colors.pinkTrainColor)
        ));


        List<RouteDetails> routes = scenario.getRoutes();

        Set<RouteDetails> processedRoutes = new HashSet<>();



        for(final TInst instance : instances)
        {
            List<RouteDetails> selectedDetails = new ArrayList<>();
            for(RouteDetails details: routes)
            {
                if(instance.isMatchingRoute(details))
                    selectedDetails.add(details);
            }


            Player player = null;
            RouteDetails matchingDetails = null;
            for (RouteDetails details: selectedDetails) {
                Player playerChosen = processor.checkPlayer(details);
                if(playerChosen != null) {
                    matchingDetails = details;
                    player = playerChosen;
                    if(player.getColor() == instance.getColor())
                        break;
                }
            }

            if(player==null )
            {
                System.out.println("ERROR: from: " + instance.endpoint1_
                        + " to: " + instance.endpoint2_
                        + " routeColor: "+  instance.getColor().toString());
            }


            assertNotNull(matchingDetails);

            if(player!=null && player.getColor()!=instance.getColor())
            {
                System.out.println("ERROR: from: " + matchingDetails.getFrom()
                        + " to: " + matchingDetails.getTo()
                        + " routeColor: "+  matchingDetails.getColor().toString()
                        + " playerColor: "+ player.getColor().toString());
            }
            if(player==null )
            {
                System.out.println("ERROR: from: " + matchingDetails.getFrom()
                        + " to: " + matchingDetails.getTo()
                        + " routeColor: "+  matchingDetails.getColor().toString());
            }
            assertNotNull(player);
            assertEquals(player.getColor(), instance.getColor());
            processedRoutes.add(matchingDetails);
        }

        for(RouteDetails route : routes)
        {
            if(processedRoutes.contains(route))
                continue;

            Player player = processor.checkPlayer(route);
            if(player!=null)
            {
                System.out.println("Error2 from: " + route.getFrom()
                        + " to: " + route.getTo()
                        + " routeColor: "+  route.getColor().toString()
                        + " playerColor: "+ player.getColor().toString());
            }
            assertNull(player);
        }
//        Imgcodecs.imwrite("E:\\nordicResultWithTrains.jpg", processor.getCornersMarked());
    }

}
